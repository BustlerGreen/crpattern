﻿// See https://aka.ms/new-console-template for more information




using System.Data.Common;
using System.Diagnostics;
using System.Diagnostics.Metrics;
using System.Reflection;

// create a trucks park for bigbag transportation
var VolvoBigBag = new Truck("volvo", "VNR300",  360f, new Cargo(60f, true, true), true);
Truck[] bigbagVolvoPark = new Truck[10];
for (int i = 0; i < 10; i++) bigbagVolvoPark[i] = VolvoBigBag.MyClone();
for (int i = 0; i < 10; i++) Console.WriteLine(bigbagVolvoPark[i].ToString() + "-" + bigbagVolvoPark[i].GetInfo());

    // create a beds track for wood transportation
    var Kamaz = new Truck("KAMAZ", "55215", 300f, new Cargo(20f, false, false), false);
var RastyRat = Kamaz.MyClone();

public interface IMyClonable<T> where T: class
{
    T MyClone();
    string GetInfo();
}

/// <summary>
/// Cargo space - could be a bed, trailer or semitrailer
/// </summary>
public class Cargo : IMyClonable<Cargo>, ICloneable
{
    public float Cap { get { return Capasity;}}
    float Capasity;
    bool Trailer;
    bool Semi;

   public Cargo(float capasity, bool semi, bool trailer)
    {
        Capasity = capasity;
        Trailer = trailer;
        Semi = semi;
    }
    public Cargo MyClone()
    {
        return new Cargo(Capasity, Trailer, Semi);
    }
    public string GetInfo()
    {
        return $"capasity - {Capasity.ToString()} , triler - {Trailer}, semi - {Semi}";
    }

    public object Clone()
    {
        return MyClone();
    }
}

/// <summary>
/// truck could be with bed or tractor
/// isSemi means that cargo should be trailer or semitrailer
/// </summary>
public class Truck : IMyClonable<Truck>
{
    string Maker;
    string Model;
    float Hp;
    Cargo _Cargo;
    bool isSemi;

    public Truck(string maker, string model, float hp, Cargo cargo, bool semi)
    {
        Maker = maker;
        Model = model;
        Hp = hp;
        _Cargo = cargo;
        isSemi = semi;
    }

    
    public Truck MyClone()
    {
        return new Truck(Maker, Model, Hp, _Cargo?.MyClone(), isSemi );
    }

    public string GetInfo()
    {
        return $"{this.Maker}, {this.Model}, {this.isSemi}";
    }

    public object Clone()
    {
        return MyClone();
    }
}